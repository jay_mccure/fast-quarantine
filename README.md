# Fast Quarantine

This project publishes simple `.txt` files that include one test entity identifier per line.
These files are then published as Pages, e.g.
https://gitlab-org.gitlab.io/quality/engineering-productivity/fast-quarantine/rspec/fast_quarantine-gitlab.txt.

The goal is to allow to fast-quarantine tests without the need to open & merge a merge request in [the main GitLab project](https://gitlab.com/gitlab-org/gitlab).

This is mostly intended for un-blocking deployments blocked due to flaky/broken tests.

To resolve a failed CI job using this method, restarting the job by itself will not automatically pick up the fast-quarantine update. Instead, we need to run a new pipeline, or rerun the `retrieve-test-metadata` job (`download-fast-quarantine-report` in e2e pipelines) to re-download the quarantine list, before retrying the failed job.

This implements the idea proposed at https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/204.

## Supported test frameworks

Fast-quarantine is supported for:

- [the main GitLab project](https://gitlab.com/gitlab-org/gitlab)'s RSpec test suite
- [the end-to-end test suite](https://gitlab.com/gitlab-org/gitlab/-/tree/master/qa/qa/specs)

## Fast-quarantine a test

- [ ] Make sure an issue exist to track the flaky test in the [the main GitLab](https://gitlab.com/gitlab-org/gitlab) issue tracker
- [ ] Edit the [`rspec/fast_quarantine-gitlab.txt`](https://gitlab.com/gitlab-org/quality/engineering-productivity/fast-quarantine/-/blob/main/rspec/fast_quarantine-gitlab.txt) file and add a test entity identifier, in one of the following format:
  - An example id, e.g. `spec/tasks/gitlab/usage_data_rake_spec.rb[1:5:2:1]`
  - A test `file` with a line number, e.g. `ee/spec/features/boards/swimlanes/epics_swimlanes_sidebar_spec.rb:42`
  - A test `file`, e.g. `ee/spec/features/boards/swimlanes/epics_swimlanes_sidebar_spec.rb`
  Note that end-to-end tests can also be fast-quarantined, but you should omit the top-level `qa/` folder.
  For instance, to fast-quarantine `qa/qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb`, you would add
  `qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb` to the
  [`rspec/fast_quarantine-gitlab.txt`](https://gitlab.com/gitlab-org/quality/engineering-productivity/fast-quarantine/-/blob/main/rspec/fast_quarantine-gitlab.txt) file.
- [ ] Commit the change to a new branch and open a merge request with a description on why the test is fast-quarantined, with a link to the flaky test issue.
- [ ] You can merge the merge request yourself or ask for review/approval/merge, as you see fit.
- [ ] To immediately unblock a pipeline, first rerun the `retrieve-test-metadata` job (`download-fast-quarantine-report` in e2e pipelines), and then retry the failed job.
